package br.com.itau;

public class Conta {
    public int numConta;
    protected String tipo;
    private String dono;
    private float saldo;
    private boolean status;
    private String mes;

    public Conta(){
        this.saldo = 0;
        this.status = true;
    }

    // getters e setters
    public int getNumConta() {
        return numConta;
    }
    public void setNumConta(int numConta) {
        this.numConta = numConta;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getDono() {
        return dono;
    }
    public void setDono(String dono) {
        this.dono = dono;
    }
    public float getSaldo() {
        return saldo;
    }
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public String getMes() {
        return mes;
    }
    public void setMes(String mes) { this.mes = mes;
    }

    // métodos
    public void estadoAtual() {
        System.out.println("Número: " + this.numConta);
        System.out.println("Tipo: " + this.tipo);
        System.out.println("Dono: " + this.dono);
        System.out.println("Saldo atual: " + this.saldo);
    }
    public void abrirConta(){
        this.setTipo(tipo);
        this.setStatus(true);
        System.out.println("Conta aberta com sucesso!");
    }
    public void depositar(float valor){
        setSaldo(getSaldo() + valor);
        System.out.println("Depósito realizado!!!");
    }
    public void sacar(float valor){
        setSaldo(getSaldo() - valor);
        System.out.println("Saque realizado!!!");
    }

}

