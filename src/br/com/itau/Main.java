package br.com.itau;

public class Main {

    public static void main(String[] args) {
	    Conta conta = new Conta();
	    conta.setNumConta(11111);
	    conta.setTipo("CC");
	    conta.setDono("Groger");
	    conta.abrirConta();
	    conta.depositar(100.0f);
	    conta.sacar(50.0f);
	    conta.estadoAtual();
    }
}
